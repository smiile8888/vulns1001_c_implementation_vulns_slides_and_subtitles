1
00:00:00,160 --> 00:00:05,839
let's talk about sanitization sanity now

2
00:00:04,000 --> 00:00:07,120
well the most important thing you can do

3
00:00:05,839 --> 00:00:10,080
to reduce the vulnerability of your

4
00:00:07,120 --> 00:00:12,320
programs is severely limiting the inputs

5
00:00:10,080 --> 00:00:14,480
that it considers valid and in fact

6
00:00:12,320 --> 00:00:16,640
nothing ruins an attacker's day quite

7
00:00:14,480 --> 00:00:19,680
like proper sanity checks

8
00:00:16,640 --> 00:00:22,720
you might even go so far as to say that

9
00:00:19,680 --> 00:00:25,199
you can neutralize ACID with a bunch of

10
00:00:22,720 --> 00:00:26,720
basic sanity checks

11
00:00:25,199 --> 00:00:28,800
that's right and the analogy works

12
00:00:26,720 --> 00:00:31,279
because some fire extinguishers use

13
00:00:28,800 --> 00:00:33,440
sodium bicarbonate a weak base

14
00:00:31,279 --> 00:00:35,120
for extinguishing fires

15
00:00:33,440 --> 00:00:37,280
now let's return to some of the root

16
00:00:35,120 --> 00:00:39,040
causes of these vulnerabilities and

17
00:00:37,280 --> 00:00:41,120
examine the sweet potato

18
00:00:39,040 --> 00:00:44,079
so one of the things that you can do to

19
00:00:41,120 --> 00:00:46,079
reduce the buffer overflows caused by

20
00:00:44,079 --> 00:00:48,640
these weakly bounded functions is to

21
00:00:46,079 --> 00:00:50,960
adopt safer and more explicit api

22
00:00:48,640 --> 00:00:52,960
alternatives where possible

23
00:00:50,960 --> 00:00:55,039
so for instance if we examine the bad

24
00:00:52,960 --> 00:00:56,640
batch which are the most common hitters

25
00:00:55,039 --> 00:00:58,719
things like memcpy string copies

26
00:00:56,640 --> 00:01:01,199
string cat etc

27
00:00:58,719 --> 00:01:02,719
well there's oftentimes safer

28
00:01:01,199 --> 00:01:04,159
alternatives available I don't want to

29
00:01:02,719 --> 00:01:06,400
say they're completely safe they're just

30
00:01:04,159 --> 00:01:08,159
safer because they're more explicit

31
00:01:06,400 --> 00:01:10,720
about what the programmer intent is

32
00:01:08,159 --> 00:01:12,960
meant to be and consequently it's easier

33
00:01:10,720 --> 00:01:15,119
to examine them to determine whether an

34
00:01:12,960 --> 00:01:16,880
attacker could manipulate that intent

35
00:01:15,119 --> 00:01:18,720
now when it comes to you know what are

36
00:01:16,880 --> 00:01:20,000
all the functions that are potentially

37
00:01:18,720 --> 00:01:22,080
vulnerable

38
00:01:20,000 --> 00:01:25,360
there I just decided to look at the

39
00:01:22,080 --> 00:01:27,520
Microsoft safer C runtime library and i

40
00:01:25,360 --> 00:01:29,360
just created a big list of here's all

41
00:01:27,520 --> 00:01:31,119
the various functions that they have

42
00:01:29,360 --> 00:01:34,079
that are available that end with

43
00:01:31,119 --> 00:01:36,640
underscore s and those are their safer

44
00:01:34,079 --> 00:01:38,400
alternative libraries so we can see

45
00:01:36,640 --> 00:01:39,759
things here like you know of course

46
00:01:38,400 --> 00:01:43,600
string cad

47
00:01:39,759 --> 00:01:45,520
sn scanf print fs

48
00:01:43,600 --> 00:01:47,680
move s

49
00:01:45,520 --> 00:01:49,840
so a lot of these are the things that

50
00:01:47,680 --> 00:01:52,240
are you know doing string handling or

51
00:01:49,840 --> 00:01:54,399
wide character string handling some of

52
00:01:52,240 --> 00:01:55,920
the underscore s things don't have to do

53
00:01:54,399 --> 00:01:57,439
with you know buffer overflows and

54
00:01:55,920 --> 00:01:59,840
things like that

55
00:01:57,439 --> 00:02:01,759
for instance I would say you know make

56
00:01:59,840 --> 00:02:04,000
temp s or something like that that's

57
00:02:01,759 --> 00:02:05,439
probably more about like race conditions

58
00:02:04,000 --> 00:02:08,239
a thing we'll learn about in future

59
00:02:05,439 --> 00:02:10,000
classes and file system type attacks but

60
00:02:08,239 --> 00:02:11,599
the basic idea is you know you get a

61
00:02:10,000 --> 00:02:14,000
sense of any of these things that are

62
00:02:11,599 --> 00:02:16,239
dealing with strings or memory moves or

63
00:02:14,000 --> 00:02:18,319
memory copies and stuff like that uh

64
00:02:16,239 --> 00:02:20,239
they probably means that

65
00:02:18,319 --> 00:02:22,319
for every underscore s there's you know

66
00:02:20,239 --> 00:02:25,280
the unsafe alternative that could

67
00:02:22,319 --> 00:02:28,080
potentially cause security problems

68
00:02:25,280 --> 00:02:29,760
now there is the misleading batch which

69
00:02:28,080 --> 00:02:31,840
are the sort of more

70
00:02:29,760 --> 00:02:35,760
standardized things there's the string n

71
00:02:31,840 --> 00:02:37,280
copy string n cat string s n printf

72
00:02:35,760 --> 00:02:40,560
and these are better than you know

73
00:02:37,280 --> 00:02:42,480
strcpy string cat etc but they can

74
00:02:40,560 --> 00:02:44,560
be misleading in the sense of just

75
00:02:42,480 --> 00:02:45,920
because you throw an end onto something

76
00:02:44,560 --> 00:02:48,319
doesn't mean that it's automatically

77
00:02:45,920 --> 00:02:50,480
safe and so let's see an example of why

78
00:02:48,319 --> 00:02:53,280
that's true so here's here was our

79
00:02:50,480 --> 00:02:55,360
original sbo the stack buffer overflow

80
00:02:53,280 --> 00:02:57,040
which just took rv and unconditionally

81
00:02:55,360 --> 00:02:59,680
copied it whatever the size of the

82
00:02:57,040 --> 00:03:02,400
string was into the buffer

83
00:02:59,680 --> 00:03:04,319
and so the string n copy is supposed to

84
00:03:02,400 --> 00:03:06,720
be the safer one and for that you say

85
00:03:04,319 --> 00:03:09,920
you know I will copy not more than n

86
00:03:06,720 --> 00:03:12,000
characters and the expectation is that

87
00:03:09,920 --> 00:03:14,239
you give an n that is equal to the size

88
00:03:12,000 --> 00:03:16,319
of your buffer so that it's not going to

89
00:03:14,239 --> 00:03:18,640
overflow the buffer but of course these

90
00:03:16,319 --> 00:03:20,800
can be just as unsafe if you know the

91
00:03:18,640 --> 00:03:23,840
ends that are chosen are you know

92
00:03:20,800 --> 00:03:25,519
arbitrary capricious and not correct so

93
00:03:23,840 --> 00:03:28,159
consequently you know these things are

94
00:03:25,519 --> 00:03:29,840
not a panacea and of themselves

95
00:03:28,159 --> 00:03:32,319
there's another problem with things like

96
00:03:29,840 --> 00:03:34,480
string end copy which is that it doesn't

97
00:03:32,319 --> 00:03:36,319
actually null terminate and so you could

98
00:03:34,480 --> 00:03:39,680
get into a situation where you know

99
00:03:36,319 --> 00:03:41,440
maybe the constant itself is okay just

100
00:03:39,680 --> 00:03:43,599
string and copy and you're limiting it

101
00:03:41,440 --> 00:03:46,159
to four characters so that's definitely

102
00:03:43,599 --> 00:03:47,840
not going to overflow buff two but then

103
00:03:46,159 --> 00:03:50,159
if it's followed up with you know some

104
00:03:47,840 --> 00:03:52,239
further unsafe operation

105
00:03:50,159 --> 00:03:54,879
well because string end copy does not

106
00:03:52,239 --> 00:03:56,879
guarantee that it null terminates well

107
00:03:54,879 --> 00:03:58,840
you could have basically four characters

108
00:03:56,879 --> 00:04:01,840
in here and whatever the rest of the

109
00:03:58,840 --> 00:04:03,439
uninitialized content of buff 2 is

110
00:04:01,840 --> 00:04:05,120
there might not be a null character

111
00:04:03,439 --> 00:04:06,879
there and so when the strcpy hits

112
00:04:05,120 --> 00:04:10,879
it it could just keep copying and

113
00:04:06,879 --> 00:04:14,000
copying well past the bounds of buff 1.

114
00:04:10,879 --> 00:04:15,760
now then there are things like stp and

115
00:04:14,000 --> 00:04:18,079
copy

116
00:04:15,760 --> 00:04:20,799
stupid copy

117
00:04:18,079 --> 00:04:23,520
and those do not terminate if the length

118
00:04:20,799 --> 00:04:25,120
is greater than string length but that

119
00:04:23,520 --> 00:04:26,880
just means that this thing will actually

120
00:04:25,120 --> 00:04:28,560
happily buffer overflow the buffer for

121
00:04:26,880 --> 00:04:31,440
you so if you look at the man page it

122
00:04:28,560 --> 00:04:33,680
says if the length of string laying of

123
00:04:31,440 --> 00:04:35,440
source is smaller than n the remaining

124
00:04:33,680 --> 00:04:37,759
characters of the array pointed to by

125
00:04:35,440 --> 00:04:40,000
dest are filled in with null bytes so

126
00:04:37,759 --> 00:04:41,520
again here if the

127
00:04:40,000 --> 00:04:43,840
n is wrong

128
00:04:41,520 --> 00:04:46,320
if you say you want a thousand bytes

129
00:04:43,840 --> 00:04:48,160
maximum copied well this is not about

130
00:04:46,320 --> 00:04:49,919
maximum this one says you know what if

131
00:04:48,160 --> 00:04:52,240
you copy less than a thousand bytes i'll

132
00:04:49,919 --> 00:04:54,639
happily fill in up to a thousand bytes

133
00:04:52,240 --> 00:04:56,160
worth of nulls afterwards and so that'll

134
00:04:54,639 --> 00:04:59,360
just go ahead and buffer overflow for

135
00:04:56,160 --> 00:05:01,759
you as well but stupid copy can also

136
00:04:59,360 --> 00:05:03,360
have the problem of non-null termination

137
00:05:01,759 --> 00:05:05,520
so if we just look one more sentence

138
00:05:03,360 --> 00:05:07,680
down on the man page it says if the

139
00:05:05,520 --> 00:05:10,160
length of string length source is

140
00:05:07,680 --> 00:05:11,919
greater than or equal to n the string

141
00:05:10,160 --> 00:05:14,479
pointed to by dest will not be null

142
00:05:11,919 --> 00:05:17,440
terminated so here for instance we could

143
00:05:14,479 --> 00:05:19,680
have it closer to correct maybe just off

144
00:05:17,440 --> 00:05:22,720
by one so it says nine

145
00:05:19,680 --> 00:05:25,199
and if you pass a small string like four

146
00:05:22,720 --> 00:05:26,560
a's then it'll happily copy it and it'll

147
00:05:25,199 --> 00:05:28,160
you know fill in

148
00:05:26,560 --> 00:05:30,080
the rest like the previous one said so

149
00:05:28,160 --> 00:05:32,000
it'll fill in the rest with null

150
00:05:30,080 --> 00:05:33,680
characters on maybe it just you know

151
00:05:32,000 --> 00:05:35,039
overflows by one and maybe that doesn't

152
00:05:33,680 --> 00:05:36,800
cause a problem

153
00:05:35,039 --> 00:05:39,280
but if on the other hand you provide you

154
00:05:36,800 --> 00:05:40,880
know too big of a thing it doesn't

155
00:05:39,280 --> 00:05:43,120
actually guarantee that it's going to

156
00:05:40,880 --> 00:05:45,120
null terminate that and then ultimately

157
00:05:43,120 --> 00:05:47,199
it's going to cause an error when it

158
00:05:45,120 --> 00:05:49,199
crashes at access time because it fails

159
00:05:47,199 --> 00:05:52,160
to find a null character as it keeps

160
00:05:49,199 --> 00:05:53,759
reading off the end of that string

161
00:05:52,160 --> 00:05:55,440
so you know the what I consider the

162
00:05:53,759 --> 00:05:57,919
least bad batch are those things like

163
00:05:55,440 --> 00:06:00,160
the Windows underscore s

164
00:05:57,919 --> 00:06:01,120
suffixed functions where they try to

165
00:06:00,160 --> 00:06:02,960
provide

166
00:06:01,120 --> 00:06:04,639
more explicit constraints on you know

167
00:06:02,960 --> 00:06:06,880
what the bounds are of the buffers that

168
00:06:04,639 --> 00:06:08,319
you're copying to and from

169
00:06:06,880 --> 00:06:10,880
that provides more

170
00:06:08,319 --> 00:06:15,280
explicitness there's also on things like

171
00:06:10,880 --> 00:06:16,319
mac os and some bsds sdr l copy for

172
00:06:15,280 --> 00:06:18,960
length

173
00:06:16,319 --> 00:06:22,319
which is a more secure version as well a

174
00:06:18,960 --> 00:06:24,560
better alternative to the n versions

175
00:06:22,319 --> 00:06:26,319
and when it comes to these safer api

176
00:06:24,560 --> 00:06:28,639
alternatives we have a whole bunch more

177
00:06:26,319 --> 00:06:30,319
details on the web page so that you can

178
00:06:28,639 --> 00:06:31,759
figure out what makes the most sense for

179
00:06:30,319 --> 00:06:33,360
you in your particular execution

180
00:06:31,759 --> 00:06:35,759
environment alright well that was sweet

181
00:06:33,360 --> 00:06:37,039
potato one GUIDance that says use the

182
00:06:35,759 --> 00:06:39,520
safer alternatives when they're

183
00:06:37,039 --> 00:06:41,600
available so sweet potato two GUIDance

184
00:06:39,520 --> 00:06:43,919
is if no better alternatives are

185
00:06:41,600 --> 00:06:46,960
available then you need to add stringent

186
00:06:43,919 --> 00:06:48,639
sanity checks to the inputs and in

187
00:06:46,960 --> 00:06:50,880
particular you want to be bounce

188
00:06:48,639 --> 00:06:53,919
checking both the data that you are

189
00:06:50,880 --> 00:06:56,560
copying from and the data that you and

190
00:06:53,919 --> 00:06:58,560
the destination where you're copying to

191
00:06:56,560 --> 00:07:00,880
and so you want to make sure you're both

192
00:06:58,560 --> 00:07:02,319
not overriding which would cause the

193
00:07:00,880 --> 00:07:03,680
sort of buffer overflow errors that we

194
00:07:02,319 --> 00:07:05,599
care about but also you want to make

195
00:07:03,680 --> 00:07:07,280
sure you're not over reading because

196
00:07:05,599 --> 00:07:09,440
that will cause a different type of

197
00:07:07,280 --> 00:07:11,440
thing like an information disclosure or

198
00:07:09,440 --> 00:07:13,599
info leak vulnerability that we'll learn

199
00:07:11,440 --> 00:07:15,599
about in future classes

200
00:07:13,599 --> 00:07:17,599
furthermore before you run out and start

201
00:07:15,599 --> 00:07:19,199
adding sanity checks willy-nilly to your

202
00:07:17,599 --> 00:07:21,360
code you want to make sure that you

203
00:07:19,199 --> 00:07:23,360
first go through the class sections on

204
00:07:21,360 --> 00:07:26,240
integer overflows and underflows and

205
00:07:23,360 --> 00:07:28,160
signiness issues because a lot of the

206
00:07:26,240 --> 00:07:30,639
sanity checks that programmers typically

207
00:07:28,160 --> 00:07:32,800
add are going to end up being bypassable

208
00:07:30,639 --> 00:07:34,720
by attackers if they're not construct

209
00:07:32,800 --> 00:07:36,400
constructed correctly

210
00:07:34,720 --> 00:07:38,880
accounting for things like integer

211
00:07:36,400 --> 00:07:41,120
overflows and silence issues now in the

212
00:07:38,880 --> 00:07:43,520
root causes we also caution that there

213
00:07:41,120 --> 00:07:45,039
can be things like wrapper functions

214
00:07:43,520 --> 00:07:46,639
where the programmer writes something

215
00:07:45,039 --> 00:07:49,440
that is effectively just a wrapper

216
00:07:46,639 --> 00:07:51,199
around a memcpy strcpy etc and

217
00:07:49,440 --> 00:07:52,800
where ultimately it's doing the same

218
00:07:51,199 --> 00:07:55,280
sort of you know

219
00:07:52,800 --> 00:07:57,759
unsanitized copy of memory from source

220
00:07:55,280 --> 00:08:00,720
to destination and so the GUIDance for

221
00:07:57,759 --> 00:08:02,560
package one is to refactor wrapper

222
00:08:00,720 --> 00:08:04,639
functions to give them the inputs that

223
00:08:02,560 --> 00:08:06,960
they actually need to do proper sanity

224
00:08:04,639 --> 00:08:08,960
checks so I'll show an example of this

225
00:08:06,960 --> 00:08:11,199
in the programming paranoid by by

226
00:08:08,960 --> 00:08:13,360
example section but you know for

227
00:08:11,199 --> 00:08:15,280
instance we had one example where there

228
00:08:13,360 --> 00:08:17,599
was a thing that was effectively a

229
00:08:15,280 --> 00:08:19,840
wrapper function doing a sort of special

230
00:08:17,599 --> 00:08:22,319
purpose strcpy but when you have a

231
00:08:19,840 --> 00:08:24,080
strcpy you only have the source and

232
00:08:22,319 --> 00:08:25,360
the destination you don't have anything

233
00:08:24,080 --> 00:08:26,960
about the lengths

234
00:08:25,360 --> 00:08:29,520
and the only way to really deal with

235
00:08:26,960 --> 00:08:32,159
that is to add something like length

236
00:08:29,520 --> 00:08:34,640
checks and so you know I know that it's

237
00:08:32,159 --> 00:08:36,959
a pain in order to refactor a bunch of

238
00:08:34,640 --> 00:08:39,039
code in order to change function

239
00:08:36,959 --> 00:08:41,279
prototype declarations and things like

240
00:08:39,039 --> 00:08:42,959
that I know that sometimes for backwards

241
00:08:41,279 --> 00:08:46,240
compatibility it may not even be

242
00:08:42,959 --> 00:08:48,880
possible but where possible

243
00:08:46,240 --> 00:08:51,200
you need to add those sort of parameters

244
00:08:48,880 --> 00:08:53,519
to the actual functions themselves so

245
00:08:51,200 --> 00:08:56,080
that they can have any chance and any

246
00:08:53,519 --> 00:08:57,680
hope of doing the appropriate sanity

247
00:08:56,080 --> 00:08:59,279
checks of course you know if you can't

248
00:08:57,680 --> 00:09:00,720
do it well then you have to go find

249
00:08:59,279 --> 00:09:02,959
every single place that function was

250
00:09:00,720 --> 00:09:05,279
used and add those sanity checks so

251
00:09:02,959 --> 00:09:07,279
ultimately it's going to be a lot more

252
00:09:05,279 --> 00:09:09,839
convenient and a lot more sound and

253
00:09:07,279 --> 00:09:11,680
robust if you can just add sanity checks

254
00:09:09,839 --> 00:09:13,440
inside of a function after changing the

255
00:09:11,680 --> 00:09:15,920
parameters as opposed to having to

256
00:09:13,440 --> 00:09:18,080
scatter sanity checks around in the code

257
00:09:15,920 --> 00:09:20,000
and that other big major root cause of

258
00:09:18,080 --> 00:09:22,160
these kind of overflows was the carrot

259
00:09:20,000 --> 00:09:24,320
cause this is the situation where you

260
00:09:22,160 --> 00:09:26,399
have a loop where the exit condition is

261
00:09:24,320 --> 00:09:28,160
attacker controlled and it's doing some

262
00:09:26,399 --> 00:09:30,160
sort of write or some sort of memory

263
00:09:28,160 --> 00:09:32,560
copy inside of the loop

264
00:09:30,160 --> 00:09:34,560
so the first thing to do with this is to

265
00:09:32,560 --> 00:09:36,399
try to say well you know first you have

266
00:09:34,560 --> 00:09:38,399
to recognize that you have a problem

267
00:09:36,399 --> 00:09:40,560
say is this an attacker controlled exit

268
00:09:38,399 --> 00:09:42,880
condition and is there memory copying

269
00:09:40,560 --> 00:09:44,959
going on inside of this loop if so can

270
00:09:42,880 --> 00:09:46,480
you reformulate the loop in a way that

271
00:09:44,959 --> 00:09:47,600
makes it so that it is not attacker

272
00:09:46,480 --> 00:09:49,279
controlled

273
00:09:47,600 --> 00:09:51,040
and so we'll give examples of this in

274
00:09:49,279 --> 00:09:53,279
the programming paranoid by example

275
00:09:51,040 --> 00:09:54,880
section later now if you can't eliminate

276
00:09:53,279 --> 00:09:57,360
the fact that this has an

277
00:09:54,880 --> 00:09:58,880
attacker-controlled exit condition well

278
00:09:57,360 --> 00:10:00,959
then you may need to do some sort of

279
00:09:58,880 --> 00:10:02,720
sanity checking before the loop in order

280
00:10:00,959 --> 00:10:05,200
to again just make sure that ultimately

281
00:10:02,720 --> 00:10:06,720
it's not going to buffer overflow

282
00:10:05,200 --> 00:10:08,480
and furthermore if you can't even do

283
00:10:06,720 --> 00:10:10,480
that because you know for whatever

284
00:10:08,480 --> 00:10:12,160
reason you know the data does not lend

285
00:10:10,480 --> 00:10:13,200
itself to being you know sanity check

286
00:10:12,160 --> 00:10:14,800
pre-loop

287
00:10:13,200 --> 00:10:17,279
well then you may have to actually put

288
00:10:14,800 --> 00:10:18,800
the sanity checks inside the loop and so

289
00:10:17,279 --> 00:10:20,160
two and three make the distinction of

290
00:10:18,800 --> 00:10:21,760
obviously you know for performance

291
00:10:20,160 --> 00:10:24,000
reasons you'd like to put it outside the

292
00:10:21,760 --> 00:10:25,680
loop to do once but if you've got no

293
00:10:24,000 --> 00:10:27,600
other choice you have to put it inside

294
00:10:25,680 --> 00:10:29,519
the loop well I know now your

295
00:10:27,600 --> 00:10:31,440
performance sense is tingling and you're

296
00:10:29,519 --> 00:10:33,200
saying oh no ascended you check inside

297
00:10:31,440 --> 00:10:34,640
the loop that's going to add so much

298
00:10:33,200 --> 00:10:35,680
performance degradation I don't want to

299
00:10:34,640 --> 00:10:37,600
do that

300
00:10:35,680 --> 00:10:39,680
well let's you know think of the wise

301
00:10:37,600 --> 00:10:41,760
words of dr donald knuth and remember

302
00:10:39,680 --> 00:10:44,240
that premature optimization is the root

303
00:10:41,760 --> 00:10:46,480
of all evil so don't dismiss it out of

304
00:10:44,240 --> 00:10:48,240
hand put the sanity check make sure you

305
00:10:46,480 --> 00:10:50,000
don't buffer overflow and then do a

306
00:10:48,240 --> 00:10:53,440
performance evaluation to see whether it

307
00:10:50,000 --> 00:10:53,440
actually makes that much of a difference

