1
00:00:00,719 --> 00:00:05,520
now most exploit mitigations that are in

2
00:00:02,800 --> 00:00:07,440
widespread usage all started as sort of

3
00:00:05,520 --> 00:00:08,800
academic topics where people were just

4
00:00:07,440 --> 00:00:10,960
trying to see you know what could we

5
00:00:08,800 --> 00:00:12,480
possibly do about these exploits and

6
00:00:10,960 --> 00:00:13,840
ultimately the ones that are in common

7
00:00:12,480 --> 00:00:15,920
usage were the ones that were found to

8
00:00:13,840 --> 00:00:18,480
be practical and they were subsequently

9
00:00:15,920 --> 00:00:20,400
adopted so one of the newest things to

10
00:00:18,480 --> 00:00:22,560
be coming out of academic and into the

11
00:00:20,400 --> 00:00:25,199
world of practical is a mechanism called

12
00:00:22,560 --> 00:00:28,560
control flow integrity and the idea here

13
00:00:25,199 --> 00:00:30,640
is that you want the system overall to

14
00:00:28,560 --> 00:00:32,800
try to verify things like calling and

15
00:00:30,640 --> 00:00:35,360
returning to functions which is control

16
00:00:32,800 --> 00:00:37,040
flow you want to verify that

17
00:00:35,360 --> 00:00:39,440
these things like return addresses on

18
00:00:37,040 --> 00:00:41,200
the stack are not actually subject to

19
00:00:39,440 --> 00:00:42,879
attacker manipulation so you're

20
00:00:41,200 --> 00:00:45,440
verifying the integrity

21
00:00:42,879 --> 00:00:47,039
now if you're a execution environment

22
00:00:45,440 --> 00:00:49,200
author such as an operating system

23
00:00:47,039 --> 00:00:51,120
virtualization or firmware author and

24
00:00:49,200 --> 00:00:53,360
you've already successfully implemented

25
00:00:51,120 --> 00:00:55,120
all those other exploit mitigations

26
00:00:53,360 --> 00:00:56,160
which you should definitely do you know

27
00:00:55,120 --> 00:00:58,719
first

28
00:00:56,160 --> 00:01:00,879
then great congratulations now this is

29
00:00:58,719 --> 00:01:03,199
the next thing for you to implement

30
00:01:00,879 --> 00:01:05,680
now cfi can be achieved both through

31
00:01:03,199 --> 00:01:07,439
pure software means as well as hardware

32
00:01:05,680 --> 00:01:10,080
augmented means but it's generally

33
00:01:07,439 --> 00:01:11,840
understood that hardware augmentation is

34
00:01:10,080 --> 00:01:15,040
what's required in order to make it

35
00:01:11,840 --> 00:01:17,520
actually perform it so it is only just

36
00:01:15,040 --> 00:01:20,479
in the most recent years started to come

37
00:01:17,520 --> 00:01:24,159
into existence for instance the iPhone

38
00:01:20,479 --> 00:01:26,560
a12 sock in 2019 has support for pointer

39
00:01:24,159 --> 00:01:29,759
authentication and the Intel tiger lake

40
00:01:26,560 --> 00:01:31,920
cpus from 2020 have support for control

41
00:01:29,759 --> 00:01:33,759
enforcement technology like I said there

42
00:01:31,920 --> 00:01:35,759
are software only mechanisms so for

43
00:01:33,759 --> 00:01:38,720
instance Microsoft added a thing called

44
00:01:35,759 --> 00:01:40,720
control flow guard in visual studio 2015

45
00:01:38,720 --> 00:01:42,880
and you can absolutely just turn that on

46
00:01:40,720 --> 00:01:45,360
right now but it was understood that you

47
00:01:42,880 --> 00:01:47,119
know control flow guard is a best effort

48
00:01:45,360 --> 00:01:49,520
mechanism that is trying to get us you

49
00:01:47,119 --> 00:01:51,200
know closer to the cfi goal and of

50
00:01:49,520 --> 00:01:53,520
course they knew that these hardware

51
00:01:51,200 --> 00:01:54,960
modifications would be coming and of

52
00:01:53,520 --> 00:01:56,479
course they actually consulted with

53
00:01:54,960 --> 00:01:58,000
Intel and worked hand-in-hand right it's

54
00:01:56,479 --> 00:02:00,159
called wIntel

55
00:01:58,000 --> 00:02:01,680
they worked with them in order to get

56
00:02:00,159 --> 00:02:03,920
the type of features and functionality

57
00:02:01,680 --> 00:02:06,799
they would want to achieve achieve

58
00:02:03,920 --> 00:02:09,360
stronger cfi so again you can't even opt

59
00:02:06,799 --> 00:02:11,360
into software only cfi unless you have

60
00:02:09,360 --> 00:02:13,360
the appropriate compiler and execution

61
00:02:11,360 --> 00:02:15,440
environment support so we'll provide on

62
00:02:13,360 --> 00:02:17,920
the website more information about where

63
00:02:15,440 --> 00:02:20,319
those combinations are available and

64
00:02:17,920 --> 00:02:22,400
also you know you can opt into this type

65
00:02:20,319 --> 00:02:24,480
of technique and yes it won't be as

66
00:02:22,400 --> 00:02:27,360
strong as possible for now but as the

67
00:02:24,480 --> 00:02:29,440
hardware starts rolling out that has cfi

68
00:02:27,360 --> 00:02:32,560
hardware support this can only get

69
00:02:29,440 --> 00:02:32,560
stronger over time

