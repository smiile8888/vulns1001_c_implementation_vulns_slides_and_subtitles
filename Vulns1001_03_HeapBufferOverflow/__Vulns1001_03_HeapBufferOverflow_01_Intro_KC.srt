1
00:00:00,080 --> 00:00:05,600
heap buffer overflow's introduction

2
00:00:02,480 --> 00:00:07,600
a heap overflow is essentially when

3
00:00:05,600 --> 00:00:09,360
too much data is written to a buffer

4
00:00:07,600 --> 00:00:11,280
that resides on the heap

5
00:00:09,360 --> 00:00:13,120
thus overflowing its bounds a

6
00:00:11,280 --> 00:00:15,040
typical example is when you have an

7
00:00:13,120 --> 00:00:17,760
attacker controlled buffer with an

8
00:00:15,040 --> 00:00:20,800
attacker controlled size writting to a

9
00:00:17,760 --> 00:00:22,000
fixed size location or destination

10
00:00:20,800 --> 00:00:24,000
now we'll go over some of our

11
00:00:22,000 --> 00:00:26,240
conventions most of which are similar to

12
00:00:24,000 --> 00:00:28,880
the stack buffer overflow

13
00:00:26,240 --> 00:00:31,119
section you should have completed by now

14
00:00:28,880 --> 00:00:33,600
grey for uninitialized

15
00:00:31,119 --> 00:00:35,520
blue for initialized red for attacker

16
00:00:33,600 --> 00:00:38,719
controlled impute data

17
00:00:35,520 --> 00:00:41,440
and this kind of red blue mixture for

18
00:00:38,719 --> 00:00:44,160
semi attacker controlled input data

19
00:00:41,440 --> 00:00:46,640
and additionally yellow which would use

20
00:00:44,160 --> 00:00:48,960
to denote data that is caused by the

21
00:00:46,640 --> 00:00:52,160
attacker to be allocated in our diagram

22
00:00:48,960 --> 00:00:53,920
conventions higher addresses will be

23
00:00:52,160 --> 00:00:55,360
bottom and lower addresses will be

24
00:00:53,920 --> 00:00:56,640
written at the top

25
00:00:55,360 --> 00:00:58,719
likewise

26
00:00:56,640 --> 00:01:02,000
most significant bytes would be on the

27
00:00:58,719 --> 00:01:04,479
rightmost side of the slide and

28
00:01:02,000 --> 00:01:07,119
the least significant byte will be

29
00:01:04,479 --> 00:01:08,880
at the left side

30
00:01:07,119 --> 00:01:10,640
data writes will be occurring from the

31
00:01:08,880 --> 00:01:13,840
lower addresses to the heap to the

32
00:01:10,640 --> 00:01:13,840
higher addresses

33
00:01:13,920 --> 00:01:19,920
for example consider we have an array

34
00:01:16,240 --> 00:01:22,479
index 0 will be located here and the

35
00:01:19,920 --> 00:01:24,240
last index will be located

36
00:01:22,479 --> 00:01:28,159
at the right side of the

37
00:01:24,240 --> 00:01:30,079
of the slides towards higher addresses

38
00:01:28,159 --> 00:01:33,280
if you have a four if you have an eight

39
00:01:30,079 --> 00:01:35,759
byte integer the least significant byte

40
00:01:33,280 --> 00:01:38,159
will be on the left side and the highest

41
00:01:35,759 --> 00:01:41,280
the most significant byte will be on the

42
00:01:38,159 --> 00:01:44,399
right side this integer represented will

43
00:01:41,280 --> 00:01:44,399
be read as follows

44
00:01:45,040 --> 00:01:49,600
strings are a little more

45
00:01:46,640 --> 00:01:51,439
straightforward with regards to

46
00:01:49,600 --> 00:01:55,600
you know compared to stack

47
00:01:51,439 --> 00:01:58,320
diagrams here /home/user/hbo just reads

48
00:01:55,600 --> 00:02:00,320
the same way as it's represented on the

49
00:01:58,320 --> 00:02:02,960
heap so now I would like to take a look

50
00:02:00,320 --> 00:02:04,640
at closely what again is heap buffer

51
00:02:02,960 --> 00:02:06,079
overflow

52
00:02:04,640 --> 00:02:08,080
now suppose we have an attacker

53
00:02:06,079 --> 00:02:10,800
controlled input buffer and the

54
00:02:08,080 --> 00:02:13,040
vulnerable buffer if we get if we can

55
00:02:10,800 --> 00:02:15,920
get the attacker controlled

56
00:02:13,040 --> 00:02:19,360
input buffer to overflow this vulnerable

57
00:02:15,920 --> 00:02:21,440
buffer and subsequently contaminate

58
00:02:19,360 --> 00:02:25,120
the target data we will say that the

59
00:02:21,440 --> 00:02:25,120
heap buffer overflow has occurred

60
00:02:31,680 --> 00:02:36,080
what are some common root causes well

61
00:02:34,080 --> 00:02:39,680
unsafe or weakly bound functions such as

62
00:02:36,080 --> 00:02:41,519
memcpy strcpy strcat sprintf most of

63
00:02:39,680 --> 00:02:43,840
which you've already seen in the stack

64
00:02:41,519 --> 00:02:46,560
buffer overflow section

65
00:02:43,840 --> 00:02:48,640
likewise sequential data writes within a

66
00:02:46,560 --> 00:02:51,280
loop with an ACID loop counter or an

67
00:02:48,640 --> 00:02:53,120
ACID loop exit condition

68
00:02:51,280 --> 00:02:54,800
now we'll walk through a trivial example

69
00:02:53,120 --> 00:02:56,959
which should also be similar to your

70
00:02:54,800 --> 00:02:59,040
stack buffer overflow example except in

71
00:02:56,959 --> 00:03:01,200
this case the buffer is allocated on the

72
00:02:59,040 --> 00:03:02,959
heap via the call to malloc with a size

73
00:03:01,200 --> 00:03:07,680
of 8.

74
00:03:02,959 --> 00:03:09,440
so we're expecting a program argument

75
00:03:07,680 --> 00:03:12,239
and we will be copying that program

76
00:03:09,440 --> 00:03:14,560
argument into the buffer notice now that

77
00:03:12,239 --> 00:03:18,080
strcopy this is an unbounded copy

78
00:03:14,560 --> 00:03:21,200
has no sizes specified so if the

79
00:03:18,080 --> 00:03:23,599
parameter does contain a string greater

80
00:03:21,200 --> 00:03:26,159
than length 8 we are likely going to

81
00:03:23,599 --> 00:03:28,319
occur um likely going to experience a

82
00:03:26,159 --> 00:03:30,080
heap buffer overflow

83
00:03:28,319 --> 00:03:32,319
now what does that look like

84
00:03:30,080 --> 00:03:35,200
now argument zero contains the pathway

85
00:03:32,319 --> 00:03:37,599
program which is potentially SACI

86
00:03:35,200 --> 00:03:40,159
because it would only really be 100%

87
00:03:37,599 --> 00:03:42,640
attacker controlled if it was

88
00:03:40,159 --> 00:03:45,680
executed by an admin which had the

89
00:03:42,640 --> 00:03:48,000
privilege to place this program into any

90
00:03:45,680 --> 00:03:50,080
arbitrary location on the path now the

91
00:03:48,000 --> 00:03:51,840
arguments are passed on the stack and

92
00:03:50,080 --> 00:03:54,319
we're going to use these three dots to

93
00:03:51,840 --> 00:03:57,120
demarcate the stack versus the heap

94
00:03:54,319 --> 00:03:59,200
now in reality that range is a lot wider

95
00:03:57,120 --> 00:04:02,480
but for this example this is suffice

96
00:03:59,200 --> 00:04:04,640
after the call to main the heap memory is

97
00:04:02,480 --> 00:04:07,360
as is it could be containing previous

98
00:04:04,640 --> 00:04:09,599
data that was originally there on it on

99
00:04:07,360 --> 00:04:11,840
on uninitialized data

100
00:04:09,599 --> 00:04:14,080
now eventually the allocation to

101
00:04:11,840 --> 00:04:16,720
allocation for the buffer occurs and

102
00:04:14,080 --> 00:04:19,440
now we have 8 bytes set aside for

103
00:04:16,720 --> 00:04:21,040
buf at the time of strcpy

104
00:04:19,440 --> 00:04:23,840
the argument

105
00:04:21,040 --> 00:04:26,720
the parameter passed to the program

106
00:04:23,840 --> 00:04:30,240
will be written into the buffer but what

107
00:04:26,720 --> 00:04:31,840
got smashed well it depends it depends

108
00:04:30,240 --> 00:04:34,560
on how far

109
00:04:31,840 --> 00:04:35,919
did the override go how full was the how

110
00:04:34,560 --> 00:04:38,400
full was the heap

111
00:04:35,919 --> 00:04:42,479
and it could have overflown into on your

112
00:04:38,400 --> 00:04:44,880
unused padding, alignment data, metadata or some

113
00:04:42,479 --> 00:04:46,720
adjacent allocation data and this most

114
00:04:44,880 --> 00:04:48,960
of these actually depend on how the

115
00:04:46,720 --> 00:04:51,360
allocator was designed some allocators

116
00:04:48,960 --> 00:04:54,080
have metadata in line with the

117
00:04:51,360 --> 00:04:56,800
allocation and some of it some some do

118
00:04:54,080 --> 00:04:58,639
have the allocations out of bounds and

119
00:04:56,800 --> 00:05:01,039
if there are any exp if there are any

120
00:04:58,639 --> 00:05:03,680
attempts to harden the allocation

121
00:05:01,039 --> 00:05:06,320
against exploits that could also affect

122
00:05:03,680 --> 00:05:07,919
what actually happens after a heap

123
00:05:06,320 --> 00:05:10,080
buffer overflow

124
00:05:07,919 --> 00:05:12,639
now it's with regards to this uncertain

125
00:05:10,080 --> 00:05:14,720
uncertainty that various techniques have

126
00:05:12,639 --> 00:05:16,639
been developed to sort of give the

127
00:05:14,720 --> 00:05:18,080
attacker a little more reliability when

128
00:05:16,639 --> 00:05:19,520
it comes to exploiting heap buffer

129
00:05:18,080 --> 00:05:21,680
overflows

130
00:05:19,520 --> 00:05:24,560
one of such is the heap spray this

131
00:05:21,680 --> 00:05:26,880
technique is a little older and was very

132
00:05:24,560 --> 00:05:28,800
common with 32-bit operating systems

133
00:05:26,880 --> 00:05:31,039
essentially the attacker creates many

134
00:05:28,800 --> 00:05:32,479
heap allocations so that most addresses

135
00:05:31,039 --> 00:05:34,639
on the heap will actually contain

136
00:05:32,479 --> 00:05:36,320
attacker control input data

137
00:05:34,639 --> 00:05:37,680
what would then typically happen was

138
00:05:36,320 --> 00:05:40,800
that

139
00:05:37,680 --> 00:05:44,000
the probability of hitting

140
00:05:40,800 --> 00:05:47,199
that attacker controlled input data post

141
00:05:44,000 --> 00:05:49,120
overflow was higher now this became less

142
00:05:47,199 --> 00:05:51,440
and less common with 64-bit operating

143
00:05:49,120 --> 00:05:52,560
system and so a new technique was

144
00:05:51,440 --> 00:05:54,880
discovered

145
00:05:52,560 --> 00:05:57,600
this is this technique is referred to as

146
00:05:54,880 --> 00:05:59,360
heap feng shui or heap grooming and the

147
00:05:57,600 --> 00:06:02,000
idea is generally that the attacker

148
00:05:59,360 --> 00:06:03,919
wants to control the layout of the heap

149
00:06:02,000 --> 00:06:06,800
such that they can be a little more

150
00:06:03,919 --> 00:06:08,800
reliable in determining or what data

151
00:06:06,800 --> 00:06:10,880
will be adjacent data at the time of

152
00:06:08,800 --> 00:06:12,560
heap buffer overflow

153
00:06:10,880 --> 00:06:15,280
it usually starts out with a bunch of

154
00:06:12,560 --> 00:06:17,600
allocations where the attacker has

155
00:06:15,280 --> 00:06:18,720
where the attacker basically wants to

156
00:06:17,600 --> 00:06:20,800
control

157
00:06:18,720 --> 00:06:21,919
where specific deallocation will take

158
00:06:20,800 --> 00:06:25,120
place because

159
00:06:21,919 --> 00:06:27,120
if they have an allocation occur we'll

160
00:06:25,120 --> 00:06:28,319
call this the victim data and then

161
00:06:27,120 --> 00:06:31,039
likewise

162
00:06:28,319 --> 00:06:33,440
they have this vulnerable

163
00:06:31,039 --> 00:06:34,400
that vulnerable buffer occur right

164
00:06:33,440 --> 00:06:37,120
beside

165
00:06:34,400 --> 00:06:40,000
the victim data then at the time of

166
00:06:37,120 --> 00:06:42,960
overflow that adjacent data which is the

167
00:06:40,000 --> 00:06:43,840
victim data will be overflowed

168
00:06:42,960 --> 00:06:46,240
and

169
00:06:43,840 --> 00:06:48,639
some useful targets for this will be

170
00:06:46,240 --> 00:06:51,520
function pointers or you know such as

171
00:06:48,639 --> 00:06:52,800
those common with C++ objects

172
00:06:51,520 --> 00:06:54,560
now in reality it's a little more

173
00:06:52,800 --> 00:06:56,800
complicated than that and oftentimes

174
00:06:54,560 --> 00:06:58,880
you'll have to set up multiple of these

175
00:06:56,800 --> 00:07:01,440
arrangements in the heap to achieve the

176
00:06:58,880 --> 00:07:03,680
proper heap layout for a successful and

177
00:07:01,440 --> 00:07:08,120
reliable exploitation

178
00:07:03,680 --> 00:07:08,120
now let's go look at some real examples

